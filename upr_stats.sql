--Total - patient requests

select
count(distinct `patient-id`,`unique-id`) as 'total_requests'
from `short-book-1`
where `auto-short` = 0 and `auto-generated` = 0
and status not in ('deleted')
and date(`created-at`) between '2019-11-01' and '2019-11-07'
and `store-id` = 21
        

--UPR - Available 12hrs

select
count(distinct `patient-id`,`unique-id`) as 'available_12hrs' 
from
(
select `store-id`, `patient-id`,`unique-id`, `created-at` as 'creation_time', 
	max(`received-at`) as 'max_receive_time',
	sum(`dummy`) as total,
	sum(`r`) as 'r',
	sum(`nr`) as 'nr',
	sum(`r`)/sum(`dummy`) as 'pc'
	from
	(
		select *,
		1 as dummy,
		case when date(`received-at`) != '0000-00-00' then 1 else 0 end as 'r',
		case when date(`received-at`) = '0000-00-00' then 1 else 0 end as 'nr'
		from `short-book-1`
		where `auto-short` = 0 and `auto-generated` = 0
		and status not in ('deleted')
		and `quantity` > 0
		and date(`created-at`) between '2019-11-01' and '2019-11-07' 
		and `store-id` = 21 
	) as x
	group by `store-id`, `patient-id`,`unique-id`, date(`created-at`)
) as c
where TIMESTAMPDIFF(HOUR, `creation_time`, `max_receive_time`)<=12 and `pc` = 1


-- UPR - Available 24hrs

select
count(distinct `patient-id`,`unique-id`) as 'available_24hrs' 
from
(
select `store-id`, `patient-id`,`unique-id`, `created-at` as 'creation_time', 
	max(`received-at`) as 'max_receive_time',
	sum(`dummy`) as total,
	sum(`r`) as 'r',
	sum(`nr`) as 'nr',
	sum(`r`)/sum(`dummy`) as 'pc'
	from
	(
		select *,
		1 as dummy,
		case when date(`received-at`) != '0000-00-00' then 1 else 0 end as 'r',
		case when date(`received-at`) = '0000-00-00' then 1 else 0 end as 'nr'
		from `short-book-1`
		where `auto-short` = 0 and `auto-generated` = 0
		and status not in ('deleted')
		and `quantity` > 0
		and date(`created-at`) between '2019-11-01' and '2019-11-07' 
		and `store-id` = 21 
	) as x
	group by `store-id`, `patient-id`,`unique-id`, date(`created-at`)
) as c
where TIMESTAMPDIFF(HOUR, `creation_time`, `max_receive_time`)<=24 and `pc` = 1


-- UPR - completed same day (system+manual)

select
count(distinct `patient-id`,`unique-id`) as 'completed_sameday'
from
(
select `store-id`, `patient-id`,`unique-id`,date(`created-at`) as 'date', 
	max(date(`completed-at`)) as 'c-d',
	sum(`dummy`) as total,
	sum(`sc`) as 'sc',
	sum(`nc`) as 'nc', 
	sum(`sc`)/sum(`dummy`) as 'system_pc',
	sum(`mc`) as 'mc',
	(sum(`sc`)+sum(`mc`))/sum(`dummy`) as 'overall_pc'
	from
	(
		select *,
		1 as dummy,
		case when date(`completed-at`) != '0000-00-00' and `completed-by`='system'  then 1 else 0 end as 'sc',
		-- case when date(`completed-at`) != '0000-00-00' then 1 else 0 end as 'sc',
		case when date(`completed-at`) = '0000-00-00' then 1 else 0 end as 'nc',
		case when date(`completed-at`)!= '0000-00-00' and `completed-by` is not null and `completed-by`!='system' then 1 else 0 end as 'mc'
		-- case when date(`completed-at`)= '0000-00-00' and `status`='completed' then 1 else 0 end as 'mc'
		from `short-book-1`
		where `auto-short` = 0 and `auto-generated` = 0
		and status not in  ('deleted')
		and date(`created-at`) between '2019-11-01' and '2019-11-07'
and `store-id` = 21
	) as x
	group by `store-id`, `patient-id`, `unique-id`, date(`created-at`)
) as c
where adddate(`date`, 0) = `c-d` and `overall_pc` = 1


-- UPR - completed next day (system+manual)

select
count(distinct `patient-id`,`unique-id`) as 'completed_nextday'
from
(
select `store-id`, `patient-id`,`unique-id`,date(`created-at`) as 'date', 
	max(date(`completed-at`)) as 'c-d',
	sum(`dummy`) as total,
	sum(`sc`) as 'sc',
	sum(`nc`) as 'nc', 
	sum(`sc`)/sum(`dummy`) as 'system_pc',
	sum(`mc`) as 'mc',
	(sum(`sc`)+sum(`mc`))/sum(`dummy`) as 'overall_pc'
	from
	(
		select *,
		1 as dummy,
		case when date(`completed-at`) != '0000-00-00' and `completed-by`='system'  then 1 else 0 end as 'sc',
		-- case when date(`completed-at`) != '0000-00-00' then 1 else 0 end as 'sc',
		case when date(`completed-at`) = '0000-00-00' then 1 else 0 end as 'nc',
		case when date(`completed-at`)!= '0000-00-00' and `completed-by` is not null and `completed-by`!='system' then 1 else 0 end as 'mc'
		-- case when date(`completed-at`)= '0000-00-00' and `status`='completed' then 1 else 0 end as 'mc'
		from `short-book-1`
		where `auto-short` = 0 and `auto-generated` = 0
		and status not in  ('deleted')
		and date(`created-at`) between '2019-11-01' and '2019-11-07'
and `store-id` = 21
	) as x
	group by `store-id`, `patient-id`, `unique-id`, date(`created-at`)
) as c
where adddate(`date`, 1) = `c-d` and `overall_pc` = 1

-- UPR - completed same day (manually completed)

select
count(distinct `patient-id`,`unique-id`) as 'manual_completed_sameday'
from
(
select `store-id`, `patient-id`,`unique-id`,date(`created-at`) as 'date', 
	max(date(`completed-at`)) as 'c-d',
	sum(`dummy`) as total,
	sum(`sc`) as 'sc',
	sum(`nc`) as 'nc', 
	sum(`sc`)/sum(`dummy`) as 'system_pc',
	sum(`mc`) as 'mc',
	(sum(`sc`)+sum(`mc`))/sum(`dummy`) as 'overall_pc'
	from
	(
		select *,
		1 as dummy,
		case when date(`completed-at`) != '0000-00-00' and `completed-by`='system'  then 1 else 0 end as 'sc',
		-- case when date(`completed-at`) != '0000-00-00' then 1 else 0 end as 'sc',
		case when date(`completed-at`) = '0000-00-00' then 1 else 0 end as 'nc',
		case when date(`completed-at`)!= '0000-00-00' and `completed-by` is not null and `completed-by`!='system' then 1 else 0 end as 'mc'
		-- case when date(`completed-at`)= '0000-00-00' and `status`='completed' then 1 else 0 end as 'mc'
		from `short-book-1`
		where `auto-short` = 0 and `auto-generated` = 0
		and status not in  ('deleted')
		and date(`created-at`) between '2019-11-01' and '2019-11-07'
and `store-id` = 21
	) as x
	group by `store-id`, `patient-id`, `unique-id`, date(`created-at`)
) as c
where adddate(`date`, 0) = `c-d` and `overall_pc` = 1 and `system_pc`<1


-- UPR - completed next day (manually completed)

select
count(distinct `patient-id`,`unique-id`) as 'manual_completed_nextday'
from
(
select `store-id`, `patient-id`,`unique-id`,date(`created-at`) as 'date', 
	max(date(`completed-at`)) as 'c-d',
	sum(`dummy`) as total,
	sum(`sc`) as 'sc',
	sum(`nc`) as 'nc', 
	sum(`sc`)/sum(`dummy`) as 'system_pc',
	sum(`mc`) as 'mc',
	(sum(`sc`)+sum(`mc`))/sum(`dummy`) as 'overall_pc'
	from
	(
		select *,
		1 as dummy,
		case when date(`completed-at`) != '0000-00-00' and `completed-by`='system'  then 1 else 0 end as 'sc',
		-- case when date(`completed-at`) != '0000-00-00' then 1 else 0 end as 'sc',
		case when date(`completed-at`) = '0000-00-00' then 1 else 0 end as 'nc',
		case when date(`completed-at`)!= '0000-00-00' and `completed-by` is not null and `completed-by`!='system' then 1 else 0 end as 'mc'
		-- case when date(`completed-at`)= '0000-00-00' and `status`='completed' then 1 else 0 end as 'mc'
		from `short-book-1`
		where `auto-short` = 0 and `auto-generated` = 0
		and status not in  ('deleted')
		and date(`created-at`) between '2019-11-01' and '2019-11-07'
and `store-id` = 21
	) as x
	group by `store-id`, `patient-id`, `unique-id`, date(`created-at`)
) as c
where adddate(`date`, 1) = `c-d` and `overall_pc` = 1 and `system_pc`<1


